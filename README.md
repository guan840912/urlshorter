## 請使用 Node.js 後端 framework 實作一個短網址服務的 API Server ，其中包含以下 endpoints：
1. 創建短網址連結
2. 進入短網址連結


### Init Start DB
```bash
$ docker-compose up -d
```

### Run Express Server
```bash
$ yarn install

$ npx nodemon source/server.ts
```

### API
Name          | Routes       | Path     | Reponse |
--------------|--------------|----------|---------|
Healtcheck    | /healthcheck | /ping    |  pong   |
Healtcheck    | /healthcheck | /hello   |  hello  |
   Create     | /            | /create  |  http://\${HOSTNAME}/\${hashName}    | 
Redirect Url  | /            | /\${hashName}    | Redirect to Path |


### Example
```bash
curl --location --request POST 'http://localhost:1337/create' \
--header 'Content-Type: application/json' \
--data-raw '{
    "token": "thisistoken",
    "url": "https://www.google.com"
}'

{message: http://localhost:1337/XXXXXXXXX}
```

## Connect to DB via mongo shell

```bash

$ mongo --host localhost:27017 -u admin -p admin --authenticationDatabase admin

> use admin
switched to db admin
> db.urlrecrod.find()
{ "_id" : ObjectId("60446516820d1780c9930892"), "urlLink" : "https://www.youtube.com", "count" : 1, "hashName" : "qzxkXG8ZWa", "__v" : 0 }
{ "_id" : ObjectId("60446536820d1780c9930893"), "urlLink" : "http://www.youtube.com", "count" : 2, "hashName" : "jGzmdv8vpK", "__v" : 0 }
> exit
bye
```

## Demo Docker-compose
```bash
$ docker-compose down

# start env
$ docker-compose -f docker-compose-prod.yml up -d

# status env
$ docker-compose -f docker-compose-prod.yml ps -a
    Name                  Command               State            Ports          
--------------------------------------------------------------------------------
linetv_api_1   docker-entrypoint.sh node  ...   Up      0.0.0.0:1337->1337/tcp  
mongo          docker-entrypoint.sh mongod      Up      0.0.0.0:27017->27017/tcp

# log api 
$ docker-compose -f docker-compose-prod.yml logs api
Attaching to linetv_api_1
api_1    | [Wed Mar 10 2021 09:58:54 GMT+0000 (Coordinated Universal Time)][INFO][Server] Server Init Try to Connect DB...
api_1    | (node:1) Warning: Accessing non-existent property 'MongoError' of module exports inside circular dependency
api_1    | (Use `node --trace-warnings ...` to show where the warning was created)
api_1    | [Wed Mar 10 2021 09:58:57 GMT+0000 (Coordinated Universal Time)][INFO][Connecting MongoDB...] Mongo Connected
api_1    | [Wed Mar 10 2021 09:58:57 GMT+0000 (Coordinated Universal Time)][INFO][Server] SERVER is running on localhost:1337

# close env
$ docker-compose -f docker-compose-prod.yml down
Stopping linetv_api_1 ... done
Stopping mongo        ... done
Removing linetv_api_1 ... done
Removing mongo        ... done
Removing network linetv_default
```
import { router }from '../source/app';
import config from "../source/config/config";
import logging from "../source/config/logging";
const request = require("supertest");
import { Mockgoose } from 'mockgoose';
import mongoose from "mongoose";
import { doesNotMatch } from 'node:assert';
const NAMESPACE = "Connecting MongoDB...";


let mockgoose = new Mockgoose(mongoose);

// Mockgoose connect function.
function ciconnect() {
  return new Promise<void>((resolve, reject) => {
        mongoose
          .connect('mongodb://admin:admin@mongo:27017/admin', config.mongo.options)
          .then((res: any) => {
            logging.info(NAMESPACE, "Mongo Connected");
            resolve();
          })
          .catch((error) => {
            logging.error(NAMESPACE, error.message, error);
          });

    });
}
function connect() {
  return new Promise<void>((resolve, reject) => {
      mockgoose.prepareStorage().then(() => {
        mongoose
          .connect(config.mongo.url, config.mongo.options)
          .then((res: any) => {
            logging.info(NAMESPACE, "Mongo Connected");
            resolve();
          })
          .catch((error) => {
            logging.error(NAMESPACE, error.message, error);
          });
      });
    });
}
if (process.env.CI === 'o'){

  describe('test', () => {

    afterEach(async function() {
      await mockgoose.helper.reset();
    });
  
    afterAll(async function() {
      mongoose.disconnect();
    })
  
    beforeAll( async () => {
      await ciconnect()
    });
    describe("POST /create not the right token CI", () => {
    
      it("Test ture token CI",async () => {
        const res =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "http://www.google.com"})
          expect(res.status).toEqual(200);
          
      });
    });
  
    describe("POST /create create one", () => {
      it("Test token right",async () => {
        const res =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "http://www.youtube.com"})
          expect(res.status).toEqual(200);
        // already exist return hashName
        const resagain =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "http://www.youtube.com"})
          expect(res.body).toEqual(resagain.body);
        const resdiffurl =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "http://www.youtube.com"})
          expect(res.body).not.toBe(resdiffurl.body);
        const redirectRes =  await request(router)
        .get(`/${resdiffurl.body.message.split('/')[3]}`)
          expect(redirectRes.status).toEqual(302 || 301);
  
        const redirectHashNameNotFound =  await request(router)
        .get(`/donotexisthashname`)
          expect(redirectHashNameNotFound.status).toEqual(404);
      });
    });
  
    describe("POST /create Invalid Url", () => {
      it("Test Invalid Url",async () => {
        const res =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "htt//www.google.com"})
          expect(res.status).toEqual(400);
      });
    });
  
    describe("POST /create Permission denied", () => {
      it("Test Permission denied",async () => {
        const res =  await request(router)
          .post("/create")
          .send({token: "mocktoken", url: "http://www.google.com"})
          expect(res.status).toEqual(401);
      });
    });
  })
}else{
  describe('test', () => {

    afterEach(async function() {
      await mockgoose.helper.reset();
    });
  
    afterAll(async function() {
      const { connections } = mongoose;
      const { childProcess } = mockgoose.mongodHelper.mongoBin;
      // kill mongod
      childProcess.kill();
      // close all connections
      for (const con of connections) {
        return con.close();
      }
      return mongoose.disconnect();
    })
  
    beforeAll( async () => {
      await connect()
    });
    describe("POST /create not the right token ", () => {
    
      it("Test ture token",async () => {
        const res =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "http://www.google.com"})
          expect(res.status).toEqual(200);
          
      });
    });
  
    describe("POST /create create one", () => {
      it("Test token right",async () => {
        const res =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "http://www.youtube.com"})
          expect(res.status).toEqual(200);
        // already exist return hashName
        const resagain =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "http://www.youtube.com"})
          expect(res.body).toEqual(resagain.body);
        const resdiffurl =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "http://www.youtube.com"})
          expect(res.body).not.toBe(resdiffurl.body);
        const redirectRes =  await request(router)
        .get(`/${resdiffurl.body.message.split('/')[3]}`)
          expect(redirectRes.status).toEqual(302 || 301);
  
        const redirectHashNameNotFound =  await request(router)
        .get(`/donotexisthashname`)
          expect(redirectHashNameNotFound.status).toEqual(404);
      });
    });
  
    describe("POST /create Invalid Url", () => {
      it("Test Invalid Url",async () => {
        const res =  await request(router)
          .post("/create")
          .send({token: "thisistoken", url: "htt//www.google.com"})
          expect(res.status).toEqual(400);
      });
    });
  
    describe("POST /create Permission denied", () => {
      it("Test Permission denied",async () => {
        const res =  await request(router)
          .post("/create")
          .send({token: "mocktoken", url: "http://www.google.com"})
          expect(res.status).toEqual(401);
      });
    });
  })
}


import mongoose, { Schema } from "mongoose";
import IUrl from "../interfaces/IUrl";

const UrlSchema: Schema = new Schema({
  count: { type: Number, required: true },
  urlLink: { type: String, required: true },
  hashName: { type: String, required: false },
});

export default mongoose.model<IUrl>("Url", UrlSchema, "urlrecrod");

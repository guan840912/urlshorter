import express from "express";
import controller from "../controllers/redirecttourl";
const router = express.Router();
router.get("/*", controller.redirectToUrl);
router.post("/create", controller.createRedirectUrl);
export = router;

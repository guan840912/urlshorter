import express from "express";
import controller from "../controllers/healthcheck";
const router = express.Router();
router.get("/ping", controller.HealthCheck);
router.get("/hello", controller.Hello);
export = router;

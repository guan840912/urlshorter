import { connect } from "./db/db-connect";
import logging from "./config/logging";
import config from "./config/config";
import { router } from "./app";
const NAMESPACE = "Server";
logging.info(NAMESPACE, `Server Init Try to Connect DB...`);
connect().then(() => {
  router.listen(config.server.port, () => {
    logging.info(
      NAMESPACE,
      `SERVER is running on ${config.server.hostname}:${config.server.port}`
    );
  });
});

import express from "express";
import bodyPaster from "body-parser";
import logging from "./config/logging";
import healthCheckRouter from "./routes/healthcheck";
import redirectUrlRouter from "./routes/redirectourl";
const NAMESPACE = "Server";
export const router = express();

/**
 * Logging the request.
 */
router.use((req: any, res: any, next: any) => {
  logging.info(
    NAMESPACE,
    `METHOD - [${req.method}], URL - [${req.url}], IP - [${req.socket.remoteAddress}]`
  );
  res.on("finish", () => {
    logging.info(
      NAMESPACE,
      `METHOD - [${req.method}], URL - [${req.url}], IP - 
  [${req.socket.remoteAddress}], STATUS - [${res.statusCode}]`
    );
  });
  next();
});
/**
 * Parse the request.
 */
router.use(bodyPaster.urlencoded({ extended: false }));
router.use(bodyPaster.json());
/**
 * Rules of our API.
 */
router.use((req: any, res: any, next: any) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Request-With, Content-Type, Accept, Authorization"
  );
  if (req.method == "OPTIONS") {
    res.header("Access-Control-Allow-Motheds", "GET PATCH DELETE POST PUT");
    return res.status(200).json({});
  }
  next();
});

/** Add Routes */
router.use("/healthcheck", healthCheckRouter);
router.use("", redirectUrlRouter);

/** Error Handling Must to after Routes */
router.use((req: any, res: any, next: any) => {
  const error = new Error("not Found");
  return res.status(404).json({
    message: error.message,
  });
});

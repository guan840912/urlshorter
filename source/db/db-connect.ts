import mongoose from "mongoose";
import config from "../config/config";
import logging from "../config/logging";
const NAMESPACE = "Connecting MongoDB...";
export function connect() {
  return new Promise<void>((resolve, reject) => {
    mongoose
      .connect(config.mongo.url, config.mongo.options)
      .then((res: any) => {
        logging.info(NAMESPACE, "Mongo Connected");
        resolve();
      })
      .catch((error) => {
        logging.error(NAMESPACE, error.message, error);
      });
  });
}

export function close() {
  logging.info(NAMESPACE, "Mongo Disconnected");
  return mongoose.disconnect();
}

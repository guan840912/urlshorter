import { Document } from "mongoose";

export default interface IUrl extends Document {
  urlLink: string;
  hashName: string;
  count: number;
}

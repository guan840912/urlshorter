import { Request, Response, NextFunction } from "express";
import logging from "../config/logging";
import * as path from "path";
import Joi from "joi";
import Hashids from "hashids";
import Url from "../modles/url-modle";
import config from "../config/config";

const CREATENAMESPACE = "Create Redirect Url Controller";
const NAMESPACE = "Redirect to Url Controller";
const hashGenerator: Hashids = new Hashids("salt", 10);
const urlValidationSchema = {
  url: Joi.string()
    .uri({ scheme: ["https", "http"] })
    .trim()
    .required(),
};

async function createRedirectUrl(
  req: Request,
  res: Response,
  next: NextFunction
) {
  logging.info(CREATENAMESPACE, "Create Redirect Url route called.");
  if (req.body.token === config.token) {
    try {
      const scheme = urlValidationSchema.url;
      const checkUrlValid = await scheme.validateAsync(req.body.url);

      const instance = await Url.findOne({ urlLink: checkUrlValid });
      // If not find urlLink already exist. Create a new one
      if (!instance) {
        const count = await Url.find();
        const url = new Url({
          urlLink: checkUrlValid,
          count: count.length + 1,
          hashName: hashGenerator.encode(count.length + 1),
        });
        const save = await url.save();
        if (save.errors) {
          logging.error(
            CREATENAMESPACE,
            `[${url.urlLink}]: urlrecrod create fail.`
          );
          return res.status(501).json({
            message: `${url.urlLink} url recrod create fail.`,
          });
        }
        logging.info(
          CREATENAMESPACE,
          `This urlLink: ${url.urlLink} recrod success created!!!`
        );
        return res.status(200).json({
          message: `http://${config.server.hostname}:${config.server.port}/${url.hashName}`,
        });
      }
      logging.info(
        CREATENAMESPACE,
        `This urlLink: ${instance!.urlLink} already exist return hashName`
      );
      return res.status(200).json({
        message: `http://${config.server.hostname}:${config.server.port}/${
          instance!.hashName
        }`,
      });
    } catch (error) {
      logging.error(CREATENAMESPACE, error);
      return res.status(400).json({
        message: "InvalidUrl",
      });
    }
  }
  logging.warn(CREATENAMESPACE, "Permission denied");
  return res.status(401).json({
    message: "Permission denied",
  });
}

async function redirectToUrl(req: Request, res: Response, next: NextFunction) {
  logging.info(NAMESPACE, "Redirect to Url route called.");

  // example get query path.
  logging.info(NAMESPACE, `qury path is:  ${path.basename(req.path)}`);
  try {
    const hashName = path.basename(req.path);
    const checkPathExist = await Url.findOne({ hashName });

    if (checkPathExist !== null) {
      logging.info(
        NAMESPACE,
        `Redirect to Url route called,now redirect to ${checkPathExist.urlLink}.`
      );
      return res.redirect(checkPathExist.urlLink);
    } else {
      logging.warn(NAMESPACE, "hashName not Found !!!");
      return res.status(404).json({
        message: "hashName not Found !!!",
      });
    }
  } catch (error) {
    logging.error(NAMESPACE, error);
  }
}

export default {
  redirectToUrl,
  createRedirectUrl,
};

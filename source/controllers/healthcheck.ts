import { Request, Response, NextFunction } from "express";
import logging from "../config/logging";
const NAMESPACE = "Health Check Controller";
const HealthCheck = (req: Request, res: Response, next: NextFunction) => {
  logging.info(NAMESPACE, "Health check route called.");
  return res.status(200).json({
    message: "pong",
  });
};

const Hello = (req: Request, res: Response, next: NextFunction) => {
  logging.info(NAMESPACE, "Health check route called.");
  return res.status(200).json({
    message: "hello",
  });
};
export default {
  HealthCheck,
  Hello,
};

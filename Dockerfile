FROM node:14.15.0
ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV
ENV MONGO_USERNAME ""
ENV MONGO_PASSWORD ""
ENV MONGO_HOST ""

RUN mkdir /app
WORKDIR /app
COPY . .
RUN  npm install -g typescript@3.9.6 && yarn install && yarn run build

EXPOSE 1337
CMD ["node", "build/server.js"]